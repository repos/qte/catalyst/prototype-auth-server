FROM php:8.1 as composer
RUN apt-get update && apt-get install -y \
    curl \
    git
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
WORKDIR /public
COPY ./public /public
RUN composer install --no-dev

FROM php:8.1
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
WORKDIR /public
COPY --from=composer /public .
ENTRYPOINT php -S 0.0.0.0:80
