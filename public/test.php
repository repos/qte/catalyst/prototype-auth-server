<!DOCTYPE html>
<html>
<head>
    <title>Test Authentication</title>
</head>
<body>
    <h1>Test Authentication</h1>
    <form id="myForm" action="" method="post">
        <button type="button" id="loginBtn">Login</button>
        <button type="button" id="getTokenBtn" disabled>Get token</button>
    </form>
    <br>
    <textarea id="responseArea" rows="10" cols="100" readonly></textarea>

    <script>
        const loginBtn = document.getElementById("loginBtn");
        const getTokenBtn = document.getElementById("getTokenBtn");
        const responseArea = document.getElementById("responseArea");

        const authServerUrl = "<?php echo getenv('AUTH_SERVER_URL') ?: '' ?>";
        if (authServerUrl == '') {
          responseArea.value = "Error: AUTH_SERVER_URL not set";
        }

        else {
          fetch(authServerUrl + "/token")
            .then(response => response.json())
            .then(token => {
              if (token != null) {
                var base64Url = token.split('.')[1];
                var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
                var jsonPayload =
                  decodeURIComponent(window.atob(base64).split('').map(function(c) {
                    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
                }).join(''));
                token = JSON.parse(jsonPayload);
              }
              if (token != null && 'name' in token) {
                loginBtn.textContent = "Logout " + token.name;
                loginBtn.addEventListener("click", function () {
                  window.location.href = authServerUrl + "/logout";
                });
                getTokenBtn.removeAttribute("disabled");
              } else {
                loginBtn.textContent = "Login";
                loginBtn.addEventListener("click", function () {
                  window.location.href = authServerUrl;
                });
                getTokenBtn.disabled = true;
              }
            })
            .catch(error => {
              responseArea.value = "Error: " + error.message;
            });

          getTokenBtn.addEventListener("click", function () {
            fetch(authServerUrl + "/token")
              .then(response => response.text())
              .then(data => {
                if (data != null) {
                  var base64Url = data.split('.')[1];
                  var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
                  var jsonPayload =
                    decodeURIComponent(window.atob(base64).split('').map(function(c) {
                      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
                  }).join(''));
                  data = jsonPayload;
                }
                responseArea.value = data;
              })
              .catch(error => {
                responseArea.value = "Error: " + error.message;
              });
          });
        }
    </script>
</body>
</html>
