<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/src/Server.php';
require __DIR__ . '/src/AuthHandler.php';
require __DIR__ . '/src/JWTHandler.php';

$frontEndUrl = getenv( 'FRONT_END_URL' );
$authServerUrl = getenv( 'AUTH_SERVER_URL' );
$idpUrl = getenv( 'IDP_URL' );
$idpClientID = getenv( 'IDP_CLIENT_ID' );
$idpClientSecret = getenv( 'IDP_CLIENT_SECRET' );

$authHandler = new AuthHandler(
	$frontEndUrl,
	$authServerUrl,
	$idpUrl,
	$idpClientID,
	$idpClientSecret
);
$jwtHandler = new JWTHandler( $idpClientSecret );
$server = new Server( $frontEndUrl, $authHandler, $jwtHandler );
$server->run();
