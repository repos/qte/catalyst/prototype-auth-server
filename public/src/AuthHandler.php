<?php

use Jumbojett\OpenIDConnectClient;
use Jumbojett\OpenIDConnectClientException;

class AuthHandler {

	private $frontEndUrl;
	private $authServerUrl;
	private $oidc;

	public function __construct(
		$frontEndUrl,
		$authServerUrl,
		$idpUrl,
		$idpClientID,
		$idpClientSecret
	) {
		session_start();

		$this->frontEndUrl = $frontEndUrl;
		$this->authServerUrl = $authServerUrl;
		$this->oidc = new OpenIDConnectClient(
			$idpUrl,
			$idpClientID,
			$idpClientSecret
		);
	}

	public function login(): void {
		if ( !$this->isLoggedIn() ) {
			$this->oidc->setRedirectURL( $this->authServerUrl );
			$this->oidc->setResponseTypes( [ 'code' ] );
			$this->oidc->addScope( 'openid' );
			$this->oidc->addScope( 'profile' );
			$this->oidc->addScope( 'email' );
			$this->oidc->addScope( 'groups' );

			// If unauthenticted, redirect to IDP;
			// otherwise handle the OIDC callback
			if ( $this->oidc->authenticate() ) {
				$_SESSION[ 'id_token' ] = $this->oidc->getIdToken();
				$_SESSION[ 'user_info' ] = $this->oidc->requestUserInfo();
			}
		}
	}

	public function logout(): void {
		if ( $this->isLoggedIn() ) {
			$idToken = $_SESSION[ 'id_token' ];
			session_unset();
			try {
				$this->oidc->signOut( $idToken, $this->frontEndUrl );
			} catch ( OpenIDConnectClientException $e ) {
				// ignore - endpoint does not support logout
			}
		}
	}

	public function getTokenPayload(): ?array {
		if ( $this->isLoggedIn() ) {
			return $this->getUserInfo();
		}
		return null;
	}

	private function isLoggedIn(): bool {
		return ( isset( $_SESSION[ 'id_token' ] ) );
	}

	private function getUserInfo(): array {
		$result = [];
		$userInfo = $_SESSION[ 'user_info' ];
		$mappings = [
			[ 'name' ],
			[ 'email' ],
			[
				'memberOf',
				'groups',
				fn( $value ) => array_map( fn( $x ) => substr( $x, 3, -30 ), $value )
			]
		];
		foreach ( $mappings as $mapping ) {
			$oldKey = $mapping[ 0 ];
			if ( isset( $userInfo->$oldKey ) ) {
				$value = $userInfo->$oldKey;
				if ( isset( $mapping[ 1 ] ) ) {
					$newKey = $mapping[ 1 ];
				} else {
					$newKey = $oldKey;
				}
				if ( isset( $mapping[ 2 ] ) ) {
					$value = $mapping[ 2 ]( $value );
				}
				$result[ $newKey ] = $value;
			}
		}
		return $result;
	}
}
