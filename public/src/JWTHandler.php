<?php

class JWTHandler {

	private $secret;

	public function __construct( $secret ) {
		$this->secret = $secret;
	}

	public function getToken( $payload ): string {
		$header = [ 'typ' => 'JWT', 'alg' => 'HS256' ];
		$base64UrlHeader = $this->base64url_encode( json_encode( $header ) );

		$now = time();
		$expiry = [
			'iat' => $now,
			'exp' => $now + 5 * 60
		];
		$payload = (object) array_merge( $payload, $expiry );
		$base64UrlPayload = $this->base64url_encode( json_encode( $payload ) );

		$signature = hash_hmac(
			'sha256',
			$base64UrlHeader . "." . $base64UrlPayload,
			$this->secret,
			true
		);
		$signature = $this->base64url_encode( $signature );

		return $base64UrlHeader . "." . $base64UrlPayload . "." . $signature;
	}

	private function base64url_encode( $value ): string {
		return str_replace(
			[ '+', '/', '=' ],
			[ '-', '_', '' ],
			base64_encode( $value )
		);
	}
}
