<?php

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Factory\AppFactory;
use Slim\Middleware\OutputBufferingMiddleware;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Routing\RouteCollectorProxy;

class Server {

	private $frontEndUrl;
	private $authHandler;
	private $jwtHandler;

	public function __construct(
		$frontEndUrl,
		AuthHandler $authHandler,
		JWTHandler $jwtHandler
	) {
		$this->frontEndUrl = $frontEndUrl;
		$this->authHandler = $authHandler;
		$this->jwtHandler = $jwtHandler;
	}

	public function run(): void {
		$app = AppFactory::create();

		$streamFactory = new StreamFactory();
		$mode = OutputBufferingMiddleware::APPEND;
		$outputBufferingMiddleware =
			new OutputBufferingMiddleware( $streamFactory, $mode );
		$app->add( $outputBufferingMiddleware );

		$app->addErrorMiddleware( true, true, true );

		$app->map(
			[ 'GET', 'POST' ],
			'/auth',
			function ( Request $request, $response ) {
				$this->authHandler->login();
				return $response
					->withHeader( 'Location', $this->frontEndUrl )
					->withStatus( 302 );
			}
		);

		$app->get(
			'/auth/logout',
			function ( Request $request, $response ) {
				$this->authHandler->logout();
				return $response
					->withHeader( 'Location', $this->frontEndUrl )
					->withStatus( 302 );
			}
		);

		$app->get(
			'/auth/token',
			function ( Request $request, $response ) {
				$tokenPayload = $this->authHandler->getTokenPayload();
				if ( $tokenPayload ) {
					$token = $this->jwtHandler->getToken( $tokenPayload );
				} else {
					$token = null;
				}
				$response->getBody()->write( json_encode( $token ) );
				return $response
					->withHeader( 'Content-Type', 'application/json' );
			}
		);

		$app->run();
	}
}
